1. Alle boeken die niet zijn geschreven door een auteur waarvan de familienaam begint met een d, k of r en die gepubliceerd zijn in de jaren zeventig en twintig van de vorige eeuw.

select Familienaam, Titel, Verschijningsjaar
   from Boeken
   where (Familienaam not in ('d', 'k', 'r'))
     and (Verschijningsjaar in ('192_', '197_'))
     
2. Alle boeken geschreven door de auteurs waarvan de familienaam begint met een d, k of r en die een boek gepubliceerd hebben in het tweede jaar van elk decennium van de vorige eeuw.

select Familienaam, Titel, Verschijningsjaar
   from Boeken
   where (Familienaam not in ('d', 'k', 'r'))
     and (Verschijningsjaar in ('1902', '1912', '1922', '1932', '1942', '1952', '1962', '1972', '1982', '1992'))